<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Todo</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>

    <?php
      if(isset($_SESSION['message'])): ?>
      
      <div class="alert alert-<?=$_SESSION['msg_type']?>">
          <?php
             echo $_SESSION['message'];
             unset($_SESSION['message']);
          ?>
      </div>
      <?php endif ?> 


    <div class="container">
    <?php 
     $mysqli = new mysqli('localhost','root','','crud') or die(mysqli_error($mysqli));
     $result = $mysqli->query("SELECT * FROM data") or die(mysqli_error($mysqli));
    ?> 
    <div d-flex>
         <table class="table">
             <thead>
                <tr>
                    <th>Name</th>
                    <th colspan="2">Actions</th>
                </tr>    
             </thead>

         <?php 
             while($row = $result->fetch_assoc()): ?>
             <tr>
                <td><?php echo $row['name']; ?></td>
                <td>
                    <a href="index.php?edit=<?php echo $row['id']; ?>"
                     class="btn btn-info">EDIT</a>
                     <a href="process.php?delete=<?php echo $row['id']; ?>"
                     class="btn btn-danger">Delete</a>
                </td>
             </tr>
             <?php endwhile; ?>
        </table>
     </div>

    <?php
      function pre_r($array){
        echo '<pre>';
        print_r($array);
        echo '</pre>';
      }
    ?>
     <div d-flex flex-row justify-content-center>
         <form action="process.php" method="POST">
            <div form-group>
              <label>Enter the Task</label>
              <input type="text" name="name" class="form-control" value="<?php echo $name; ?>" placeholder="Enter Your todo's"> 
            </div>
            <div>
            <button type="submit" name="save" class="btn btn-primary">Save </button>
            </div>

        </form> 
    </div>

    </div> 
</body>
</html>