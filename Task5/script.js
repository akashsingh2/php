const promise = new Promise((resolve) => {
    setTimeout(()=> {
        resolve(2);
    }, 2000)
})

promise.then((value) => console.log(value));